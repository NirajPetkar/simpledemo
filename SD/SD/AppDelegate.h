//
//  AppDelegate.h
//  SD
//
//  Created by niraj on 09/02/18.
//  Copyright © 2018 CoreflexSolutions. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

