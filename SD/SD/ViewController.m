//
//  ViewController.m
//  SD
//
//  Created by niraj on 09/02/18.
//  Copyright © 2018 CoreflexSolutions. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [MSAppCenter start:@"d75b2d04-827a-4ded-bb68-3a0066101cad" withServices:@[[MSAnalytics class], [MSCrashes class]]];
    
    [MSCrashes setDelegate:self];
    // Do any additional setup after loading the view, typically from a nib.
    
    
}
- (IBAction)buttonTestCrashTap:(id)sender
{
    [MSCrashes generateTestCrash];
}

- (BOOL)crashes:(MSCrashes *)crashes shouldProcessErrorReport:(MSErrorReport *)errorReport {
    return YES; // return YES if the crash report should be processed, otherwise NO.
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
